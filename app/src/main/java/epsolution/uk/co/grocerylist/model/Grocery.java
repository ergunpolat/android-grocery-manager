package epsolution.uk.co.grocerylist.model;

/**
 * Created by Ergun Polat on 20/08/2017.
 */

public class Grocery {
    private String name;
    private String quantity;
    private String dateItemAdded;
    private int id;

    public Grocery() {
    }

    public Grocery(String name, String quantity, String dateItemAdded, int id) {
        this.name = name;
        this.quantity = quantity;
        this.dateItemAdded = dateItemAdded;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDateItemAdded() {
        return dateItemAdded;
    }

    public void setDateItemAdded(String dateItemAdded) {
        this.dateItemAdded = dateItemAdded;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Grocery{" +
                "name='" + name + '\'' +
                ", quantity='" + quantity + '\'' +
                ", dateItemAdded='" + dateItemAdded + '\'' +
                ", id=" + id +
                '}';
    }
}
