package epsolution.uk.co.grocerylist.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import epsolution.uk.co.grocerylist.R;
import epsolution.uk.co.grocerylist.model.Grocery;
import epsolution.uk.co.grocerylist.service.DatabaseHandler;
import epsolution.uk.co.grocerylist.ui.RecyclerViewAdapter;

public class ListActivity extends AppCompatActivity {
    private static final String TAG = ListActivity.class.getSimpleName();
    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mRecyclerViewAdapter;
    private List<Grocery> mGroceries;
    private List<Grocery> mList;
    private DatabaseHandler db;

    private AlertDialog.Builder mDialogBuilder;
    private AlertDialog mDialog;
    private EditText groceryItem;
    private EditText quantity;
    private Button saveBtn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                createPopupDialog();
            }
        });

        db = new DatabaseHandler(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleViewId);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mGroceries = new ArrayList<>();
        mList = new ArrayList<>();

        // Get items from DB;
        mGroceries = db.getAllGroceries();

        for (Grocery g : mGroceries) {
            Grocery grocery = new Grocery();
            grocery.setId(g.getId());
            grocery.setName(g.getName());
            grocery.setQuantity("Qty: " + g.getQuantity());
            grocery.setDateItemAdded("added on: "+g.getDateItemAdded());

            mList.add(grocery);
        }

        mRecyclerViewAdapter = new RecyclerViewAdapter(this,mList);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerViewAdapter.notifyDataSetChanged();

    }


    public void createPopupDialog() {
        mDialogBuilder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.popup, null);
        groceryItem =  view.findViewById(R.id.groceryItem);
        quantity = view.findViewById(R.id.groceryQty);
        saveBtn = view.findViewById(R.id.saveBtn);

        mDialogBuilder.setView(view);
        mDialog = mDialogBuilder.create();
        mDialog.show();


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!groceryItem.getText().toString().isEmpty()
                        && !quantity.getText().toString().isEmpty()) {
                    saveGroceryToDB(view);
                }
            }
        });
    }

    private void saveGroceryToDB(View view){
        Grocery grocery = new Grocery();
        String newGrocery = groceryItem.getText().toString();
        String newQuantity = quantity.getText().toString();

        grocery.setName(newGrocery);
        grocery.setQuantity(newQuantity);

        db.addGrocery(grocery);

        Snackbar.make(view, "Item Saved!" , Snackbar.LENGTH_SHORT).show();
        Log.d(TAG, "Count " + db.getGroceriesCount());

        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        mDialog.dismiss();
                        startActivity(new Intent(ListActivity.this, ListActivity.class));
                    }
                }, 1200);
    }



}
