package epsolution.uk.co.grocerylist.service;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import epsolution.uk.co.grocerylist.model.Grocery;
import epsolution.uk.co.grocerylist.utils.Constants;

/**
 * Created by Ergun Polat on 20/08/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = DatabaseHandler.class.getSimpleName();

    private Context mContext;

    public DatabaseHandler(Context context) {
        super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String CREATE_GROCERY_TABLE = "CREATE TABLE "+ Constants.TABLE_NAME
                + "(" + Constants.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Constants.KEY_GROCERY_ITEM + " TEXT, "
                + Constants.KEY_QTY_NUMBER + " TEXT,"
                + Constants.KEY_DATE_NAME + " LONG" + ");";

        sqLiteDatabase.execSQL(CREATE_GROCERY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    /**
     * CRUD operations
     */

    public void addGrocery(Grocery grocery) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Constants.KEY_GROCERY_ITEM, grocery.getName());
        values.put(Constants.KEY_QTY_NUMBER, grocery.getQuantity());
        values.put(Constants.KEY_DATE_NAME, System.currentTimeMillis());

        //insert row
        db.insert(Constants.TABLE_NAME, null, values);
        Log.d(TAG,"Saved to DB");
    }

    public Grocery getGrocery(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(Constants.TABLE_NAME, new String[] {
                Constants.KEY_ID,
                Constants.KEY_GROCERY_ITEM,
                Constants.KEY_QTY_NUMBER,
                Constants.KEY_DATE_NAME },
                Constants.KEY_ID + "=?",new String[] {String.valueOf(id)},
                null, null ,null,null);

        if(cursor != null)
            cursor.moveToFirst();

            Grocery grocery = new Grocery();
            grocery.setId(Integer.parseInt(cursor.getString(
                                    cursor.getColumnIndex(Constants.KEY_ID))));

            grocery.setName(cursor.getString(cursor.getColumnIndex(Constants.KEY_GROCERY_ITEM)));
            grocery.setQuantity(cursor.getString(cursor.getColumnIndex(Constants.KEY_QTY_NUMBER)));

            //convert timestamp to readable
            java.text.DateFormat dateFormat = java.text.DateFormat.getDateInstance();
            String formattedDate = dateFormat
                    .format(new Date(cursor
                            .getLong(cursor.getColumnIndex(Constants.KEY_DATE_NAME))).getTime());

            grocery.setDateItemAdded(formattedDate);

        return grocery;
    }

    public List<Grocery> getAllGroceries(){
        SQLiteDatabase db = this.getReadableDatabase();
        List<Grocery> groceryList = new ArrayList<>();
        Cursor cursor = db.query(Constants.TABLE_NAME, new String[]{
                Constants.KEY_ID,
                Constants.KEY_GROCERY_ITEM,
                Constants.KEY_QTY_NUMBER,
                Constants.KEY_DATE_NAME },
                null, null,null,null,Constants.KEY_DATE_NAME + " DESC");

        if(cursor.moveToFirst()) {
            do {
                Grocery grocery = new Grocery();
                grocery.setId(Integer.parseInt(cursor.getString(
                        cursor.getColumnIndex(Constants.KEY_ID))));

                grocery.setName(cursor.getString(cursor.getColumnIndex(Constants.KEY_GROCERY_ITEM)));
                grocery.setQuantity(cursor.getString(cursor.getColumnIndex(Constants.KEY_QTY_NUMBER)));

                //convert timestamp to readable
                java.text.DateFormat dateFormat = java.text.DateFormat.getDateInstance();
                String formattedDate = dateFormat
                        .format(new Date(cursor
                                .getLong(cursor.getColumnIndex(Constants.KEY_DATE_NAME))).getTime());

                grocery.setDateItemAdded(formattedDate);

                groceryList.add(grocery);

            } while (cursor.moveToNext());
        }

        return groceryList;
    }

    public int updateGrocery(Grocery grocery){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Constants.KEY_ID, grocery.getId());
        values.put(Constants.KEY_GROCERY_ITEM, grocery.getName());
        values.put(Constants.KEY_QTY_NUMBER, grocery.getQuantity());
        values.put(Constants.KEY_DATE_NAME, System.currentTimeMillis());
        return db.update(Constants.TABLE_NAME,values,Constants.KEY_ID + "=?",new String[] {
                String.valueOf(grocery.getId())
        });
    }

    public int deleteGrocery(int id){
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(Constants.TABLE_NAME,
                Constants.KEY_ID + "=?",
                new String[] {String.valueOf(id)});

    }

    public int getGroceriesCount(){
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery = "SELECT * FROM " + Constants.TABLE_NAME;
        Cursor cursor = db.rawQuery(countQuery,null);

        return cursor.getCount();
    }
}


