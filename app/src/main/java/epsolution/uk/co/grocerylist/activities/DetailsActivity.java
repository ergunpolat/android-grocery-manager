package epsolution.uk.co.grocerylist.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import epsolution.uk.co.grocerylist.R;

public class DetailsActivity extends AppCompatActivity {

    private TextView itemName;
    private TextView qty;
    private TextView dateAdded;
    private int groceryId;
    private Bundle mBundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        mBundle = getIntent().getExtras();

        itemName = (TextView) findViewById(R.id.detName);
        qty = (TextView) findViewById(R.id.detQuantity);
        dateAdded = (TextView) findViewById(R.id.detDateAdded);

        if( mBundle != null) {
            itemName.setText(mBundle.getString("name"));
            qty.setText(mBundle.getString("qty"));
            dateAdded.setText(mBundle.getString("added"));
            groceryId = mBundle.getInt("id");

        }


    }
}
