package epsolution.uk.co.grocerylist.utils;

/**
 * Created by Ergun Polat on 20/08/2017.
 */

public class Constants {

    public static final int DB_VERSION = 3;
    public static final String DB_NAME = "groceryListDB";
    public static final String TABLE_NAME = "groceryTBL";

    //Table columns
    public static final String KEY_ID = "id";
    public static final String KEY_GROCERY_ITEM = "grocery_name";
    public static final String KEY_QTY_NUMBER = "quantity_number";
    public static final String KEY_DATE_NAME = "date_added";


}
