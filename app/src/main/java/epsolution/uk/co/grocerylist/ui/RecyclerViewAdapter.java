package epsolution.uk.co.grocerylist.ui;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import epsolution.uk.co.grocerylist.R;
import epsolution.uk.co.grocerylist.activities.DetailsActivity;
import epsolution.uk.co.grocerylist.model.Grocery;
import epsolution.uk.co.grocerylist.service.DatabaseHandler;

/**
 * Created by Ergun Polat on 20/08/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private static final String TAG = RecyclerViewAdapter.class.getSimpleName();
    private Context mContext;
    private List<Grocery> mGroceryList;
    private AlertDialog.Builder mAlertDialogBuilder;
    private AlertDialog mAlertDialog;

    private LayoutInflater mLayoutInflater;

    public RecyclerViewAdapter(Context context, List<Grocery> groceryList) {
        mContext = context;
        mGroceryList = groceryList;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row, parent, false);
        return new ViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {

        Grocery grocery = mGroceryList.get(position);

        holder.groceryItemName.setText(grocery.getName());
        holder.quantity.setText(grocery.getQuantity());
        holder.dateAdded.setText(grocery.getDateItemAdded());


    }

    @Override
    public int getItemCount() {
        return mGroceryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView groceryItemName;
        public TextView quantity;
        public TextView dateAdded;
        public Button editButton;
        public Button deleteButton;
        public int id;

        public ViewHolder(final View itemView, final Context ctx) {
            super(itemView);
            mContext = ctx;

            groceryItemName = itemView.findViewById(R.id.name);
            quantity = itemView.findViewById(R.id.quantity);
            dateAdded = itemView.findViewById(R.id.dateAdded);
            editButton = itemView.findViewById(R.id.editButton);
            deleteButton = itemView.findViewById(R.id.deleteButton);
            editButton.setOnClickListener(this);
            deleteButton.setOnClickListener(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int itemPosition = getAdapterPosition();

                    Grocery grocery = mGroceryList.get(itemPosition);

                    Intent intent = new Intent(mContext, DetailsActivity.class);
                    intent.putExtra("name",grocery.getName());
                    intent.putExtra("qty",grocery.getQuantity());
                    intent.putExtra("id",grocery.getId());
                    intent.putExtra("added",grocery.getDateItemAdded());

                    mContext.startActivity(intent);

                }
            });

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.editButton: {

                    int position = getAdapterPosition();
                    Grocery grocery = mGroceryList.get(position);

                    editItem(grocery);

                    break;
                }
                case R.id.deleteButton: {
                    int position = getAdapterPosition();
                    Grocery grocery = mGroceryList.get(position);
                    deleteItem(grocery.getId());
                    break;
                }

                default: {
                    break;
                }
            }
        }


        public void deleteItem(final int id){

            mAlertDialogBuilder = new AlertDialog.Builder(mContext);
            mLayoutInflater = LayoutInflater.from(mContext);
            View view = mLayoutInflater.inflate(R.layout.confirmation_dialog, null);

            Button noBtn =  view.findViewById(R.id.noButton);
            Button yesBtn =  view.findViewById(R.id.yesButton);

            mAlertDialogBuilder.setView(view);
            mAlertDialog = mAlertDialogBuilder.create();
            mAlertDialog.show();

            noBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mAlertDialog.dismiss();
                }
            });


            yesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DatabaseHandler db = new DatabaseHandler(mContext);
                    int deleted = db.deleteGrocery(id);
                    Log.d(TAG, "Deleted: "+ deleted);
                    mGroceryList.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    mAlertDialog.dismiss();
                }
            });
        }


        public void editItem(final Grocery grocery) {
            mAlertDialogBuilder = new AlertDialog.Builder(mContext);
            mLayoutInflater = LayoutInflater.from(mContext);
            final View view = mLayoutInflater.inflate(R.layout.popup, null);

            final EditText groceryItem = view.findViewById(R.id.groceryItem);
            final EditText quantity = view.findViewById(R.id.groceryQty);
            final TextView title = view.findViewById(R.id.title);
            title.setText("Edit Grocery");
            Button saveBtn = view.findViewById(R.id.saveBtn);


            mAlertDialogBuilder.setView(view);
            mAlertDialog = mAlertDialogBuilder.create();
            mAlertDialog.show();

            saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatabaseHandler db = new DatabaseHandler(mContext);
                    grocery.setName(groceryItem.getText().toString());
                    grocery.setQuantity(quantity.getText().toString());

                    if (!groceryItem.getText().toString().isEmpty()
                            && !quantity.getText().toString().isEmpty()) {
                        int update = db.updateGrocery(grocery);
                        Log.d(TAG, "Updated "+ update);
                        notifyItemChanged(getAdapterPosition(),grocery);

                    } else {
                        Snackbar.make(view, "Add Grocery and Quantity", Snackbar.LENGTH_LONG).show();
                    }

                    mAlertDialog.dismiss();
                }
            });


        }

    }
}
